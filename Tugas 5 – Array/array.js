// Tugas Array

//Soal No. 1 (Range)
function range (startNum = 0, finishNum = 0)
{
  var data = []

  if(!startNum || !finishNum) {
    return -1
  }

  if(finishNum > startNum) {
    while (startNum <= finishNum) {
      data.push(startNum)
      startNum++
    }
  } else {
    while (startNum >= finishNum) {
      data.push(startNum)
      startNum--
    }
  }

  return data
}

console.log('Soal No. 1 (Range)')
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

//Soal No. 2 (Range with Step)
function rangeWithStep (startNum = 0, finishNum = 0, step = 1)
{
  var data = []

  if(!startNum || !finishNum || !step) {
    return -1
  }

  if(finishNum > startNum) {
    while (startNum <= finishNum) {
      data.push(startNum)
      startNum += step
    }
  } else {
    while (startNum >= finishNum) {
      data.push(startNum)
      startNum -= step
    }
  }

  return data
}

console.log('Soal No. 2 (Range with Step)')
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

//Soal No. 3 (Sum of Range)
function sum (startNum = 0, finishNum = 0, step = 1)
{
  var data = 0

  if(finishNum > startNum) {
    while (startNum <= finishNum) {
      data += startNum
      startNum += step
    }
  } else {
    while (startNum >= finishNum) {
      data += startNum
      startNum -= step
    }
  }

  return data
}

console.log('Soal No. 3 (Sum of Range)')
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

//Soal No. 4 (Array Multidimensi)

function dataHandling (input)
{
  var data = 0

  while (data < input.length) {
    console.log(
      `Nomor ID: ${input[data][0]} \nNama Lengkap:  ${input[data][1]} \nTTL:  ${input[data][2]} ${input[data][3]} \nHobi:  ${input[data][4]} \n`)
    data++
  }
}

var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun']
]

console.log('Soal No. 4 (Array Multidimensi)')
dataHandling(input)

//Soal No. 5 (Balik Kata)
function balikKata (input)
{
  var data        = ''
  var inputLength = input.length - 1

  while (inputLength >= 0) {
    data += input[inputLength]
    inputLength--
  }

  return data
}

console.log('Soal No. 5 (Balik Kata)')
console.log(balikKata('Kasur Rusak')) // kasuR rusaK
console.log(balikKata('SanberCode')) // edoCrebnaS
console.log(balikKata('Haji Ijah')) // hajI ijaH
console.log(balikKata('racecar')) // racecar
console.log(balikKata('I am Sanbers')) // srebnaS ma I

//Soal No. 6 (Metode Array)
function dataHandling2 (input)
{
  input.splice(1, 1)
  input.splice(1, 0, 'Roman Alamsyah Elsharawy')
  input.splice(2, 1)
  input.splice(2, 0, 'Provinsi Bandar Lampung')
  input.splice(4, 1)
  input.splice(4, 0, 'Pria')
  input.splice(5, 0, 'SMA Internasional Metro')

  var date = input[3].split('/')

  var bulan = parseInt(date[1])
  switch (bulan) {
    case 1: {
      bulan = 'Januari'
      break
    }
    case 2: {
      bulan = 'Februari'
      break
    }
    case 3: {
      bulan = 'Maret'
      break
    }
    case 4: {
      bulan = 'April'
      break
    }
    case 5: {
      bulan = 'Mei'
      break
    }
    case 6: {
      bulan = 'Juni'
      break
    }
    case 7: {
      bulan = 'Juli'
      break
    }
    case 8: {
      bulan = 'Agustus'
      break
    }
    case 9: {
      bulan = 'September'
      break
    }
    case 10: {
      bulan = 'Oktober'
      break
    }
    case 11: {
      bulan = 'November'
      break
    }
    case 12: {
      bulan = 'Desember'
      break
    }
    default: { console.log('Bulan tidak ditemukan') }
  }

  var join = date.join('-')
  date.sort(function (value1, value2) { return value2 - value1 })

  console.log(input)
  console.log(bulan)
  console.log(date)
  console.log(join)
  console.log(String(input[1]).slice(0, 14))
}

console.log('Soal No. 6 (Metode Array)')
var input2 = ['0001', 'Roman Alamsyah ', 'Bandar Lampung', '21/05/1989', 'Membaca']
dataHandling2(input2)
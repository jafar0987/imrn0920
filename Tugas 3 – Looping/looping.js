// Tugas – Looping

////Soal No. 1 (Looping While)

//Looping pertama

console.log('LOOPING PERTAMA')
var data = 2
while (data < 22) {
  console.log(`${data} - I love coding`)
  data += 2
}
//Looping kedua

console.log('LOOPING Kedua')
var data2 = 20
while (data2 > 0) {
  console.log(`${data2} - I will become a mobile developer`)
  data2 -= 2
}

////Soal No. 2 (Looping For)

for (var data = 1; data < 21; data++) {
  if(data % 3 == 0 && data % 2 != 0) {
    console.log(`${data} - I Love Coding`)
  } else if(data % 2 != 0) {
    console.log(`${data} - Santai`)
  } else {
    console.log(`${data} - Berkualitas`)
  }
}

////Soal No. 3(Membuat Persegi Panjang #)
var horizontal = 0
while (horizontal < 4) {
  var vertical = 0, output = ''

  while (vertical < 8) {
    output += '#'
    vertical++
  }

  console.log(output)
  horizontal++
}

////Soal No. 4(Membuat Tangga)

var horizontal = 1
while (horizontal < 8) {
  var vertical = 0, output = ''

  while (vertical < horizontal) {
    output += '#'
    vertical++
  }

  console.log(output)
  horizontal++
}

////Soal No. 5(Membuat Tangga)

var horizontal = 0
while (horizontal < 8) {
  var vertical = 0, output = ''
  while (vertical < 8) {
    output += (horizontal + vertical) % 2 == 0 ? ' ' : '#'
    vertical++
  }

  console.log(output)
  horizontal++
}
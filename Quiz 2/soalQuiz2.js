/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 *
 * Selamat mengerjakan
 */

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
class Score {
  constructor (subject, points, email)
  {
    this._subject = subject
    this._points  = points
    this._email   = email
  }

  averange ()
  {
    let sum = 0
    if(Array.isArray(this._points)) {
      let i = 0
      while (i <= this._points.length - 1) {
        sum += this._points[i]
        i++
      }
      sum = sum / this._points.length
    } else {
      sum = this._points
    }
    return sum
  }
}

score = new Score('es6', [3, 4, 1], ' rohmadijafar@gmail.com')
console.log(score.averange())
/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ['email', 'quiz-1', 'quiz-2', 'quiz-3'],
  ['abduh@mail.com', 78, 89, 90],
  ['khairun@mail.com', 95, 85, 88],
  ['bondra@mail.com', 70, 75, 78],
  ['regi@mail.com', 91, 89, 93]
]

viewScores = (data, subject) => {
  let obj             = []
  let [key, ...value] = data
  for (let i in value) {
    obj[i]         = {}
    obj[i].email   = value[i][0]
    obj[i].subject = subject
    if(subject == key[1]) {
      obj[i].points = value[i][1]
    } else if(subject == key[2]) {
      obj[i].points = value[i][2]
    } else {
      obj[i].points = value[i][3]
    }
  }
  console.log(obj)
}

// TEST CASE
viewScores(data, 'quiz-1')
viewScores(data, 'quiz-2')
viewScores(data, 'quiz-3')

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

recapScores = (data) => {
  let [key, ...value] = data
  for (let i in value) {
    let [email, value1, value2, value3] = value[i]
    let sum                             = (value1 + value2 + value3) / 3
    let predikat                        = ''
    if(sum > 90) {
      predikat = 'honour'
    } else if(sum > 80) { predikat = 'graduate'} else if(sum > 70) {
      predikat = 'participant'
    } else {
      predikat = 'failed'
    }

    console.log(
      `${parseInt(i) + 1}. Email: ${email}
    Rata-rata: ${sum}
    Predikat: ${predikat}`
    )
  }

}

recapScores(data)

// Tugas 6 – Object Literal

//Soal No. 1 (Array to Object)
arrayToObject = (arr) => {
  var arrCount = arr.length - 1, count = 0, now = new Date(), thisYear = now.getFullYear(), data = {}

  while (count <= arrCount) {
    data[`${count + 1}. ${arr[count][0]} ${arr[count][1]}`] = {
      'firstName': arr[count][0],
      'lastName': arr[count][1],
      'gender': arr[count][2],
      'age': parseInt(arr[count][3]) && parseInt(arr[count][3]) < parseInt(thisYear) ? parseInt(thisYear) - parseInt(
        arr[count][3]) : 'Invalid Birth Year'
    }
    count++
  }

  console.log(data)
}

var people = [['Bruce', 'Banner', 'male', 1975], ['Natasha', 'Romanoff', 'female']]
arrayToObject(people)

var people2 = [['Tony', 'Stark', 'male', 1980], ['Pepper', 'Pots', 'female', 2023]]
arrayToObject(people2)

arrayToObject([])

//Soal No. 2 (Shopping Time)
shoppingTime = (memberId, money) => {
  if(!memberId) {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }

  if(money < 50000) {
    return 'Mohon maaf, uang tidak cukup'
  }

  var restMoney = money, listPurchased = []
  var barang    = {
    'Sepatu Stacattu': 1500000,
    'Baju Zoro': 500000,
    'Baju H&N': 250000,
    'Sweater Uniklooh': 175000,
    'Casing Handphone': 50000
  }

  for (var i in barang) {
    if(restMoney >= barang[i]) {
      restMoney = restMoney - barang[i]
      listPurchased.push(i)
    }
  }

  return {
    'memberId': memberId,
    'money': money,
    'listPurchased': listPurchased,
    'changeMoney': restMoney
  }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

//Soal No 3
naikAngkot = (arrPenumpang) => {
  var data = []
  if(!arrPenumpang) {
    return data
  }

  var rute = ['A', 'B', 'C', 'D', 'E', 'F'], biaya = 2000, count = 0, arrCount = arrPenumpang.length - 1, jarak = 0
  while (count <= arrCount) {
    jarak = rute.indexOf(arrPenumpang[count][2]) - rute.indexOf(arrPenumpang[count][1])
    if(jarak <= 0) {
      return data
    }

    data[count] = {
      'penumpang': arrPenumpang[count][0],
      'naikDari': arrPenumpang[count][1],
      'tujuan': arrPenumpang[count][2],
      'bayar': parseInt(jarak) * parseInt(biaya)
    }
    count++
  }

  return data
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([])) //[]
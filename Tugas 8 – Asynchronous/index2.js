var readBooksPromise = require('./promise.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

var time = 10000
var i    = 0

function buku (time)
{
  if(books[i] !== undefined) {
    readBooksPromise(time, books[i]).then(function (times) {
      i++
      buku(times)
    })
      .catch(function (times) {
        console.log(`saya butuh tambahan waktu sekitar ${times/-1000} detik`)
      })
  }
}

buku(time)
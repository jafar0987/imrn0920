// di index.js
var readBooks = require('./callback.js')

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 }
]

var time = 10000
var i    = 0

function buku (time)
{
  if(books[i] != undefined) {
    readBooks(time, books[i], function (sisaWaktu) {
      i++
      buku(sisaWaktu)
    })
  }
}

buku(time)